﻿using biblioteca.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace biblioteca.Controllers
{
    [Route("api/libros")]
    [ApiController]
    public class LibrosController : ControllerBase
    {
        private readonly BibliotecaDBContext _dbContext;

        public LibrosController(BibliotecaDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: api/libros
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Libro>>> GetLibros()
        {
            if (_dbContext.Libros == null){ return NotFound(); }
            
            return await _dbContext.Libros.ToListAsync();
        }

        // GET api/libros/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Libro>> GetLibro(int id)
        {
            if (_dbContext.Libros == null) { return NotFound(); }
            var libro = await _dbContext.Libros.FindAsync(id);
            if(libro == null) { return NotFound(); }

            return libro;
        }

        // POST api/libros
        [HttpPost]
        public async Task<ActionResult<Libro>> PostLibro(Libro libro)
        {
            _dbContext.Libros.Add(libro);
            await _dbContext.SaveChangesAsync();

            return CreatedAtAction(nameof(GetLibro), new { id = libro.Id }, libro);
        }

        // DELETE api/libro/id
        [HttpDelete]
        public async Task<IActionResult> DeleteLibro(int id)
        {
            if (_dbContext.Libros == null) { return NotFound(); }
            var libro = await _dbContext.Libros.FindAsync(id);
            if (libro == null) { return NotFound(); }

            _dbContext.Libros.Remove(libro);
            await _dbContext.SaveChangesAsync();

            return NoContent();
        }
    }
}
