﻿using Microsoft.EntityFrameworkCore;

namespace biblioteca.Models
{
    public class BibliotecaDBContext: DbContext
    {
        public BibliotecaDBContext(DbContextOptions<BibliotecaDBContext> options)
            :base(options)
        {
        
        }

        public DbSet<Libro> Libros { get; set; } = null!;
    }
}
